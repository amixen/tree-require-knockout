/**
 * Created by amixen on 14.01.2015.
 */
;
define(['../config'], function(config){
	console.log('Timer loaded');

	var timestamp;

	function init(){
		timestamp = new Date();
	}

	function getTime(){
		return new Date() - timestamp;
	}

	return {
		init: init
		,getTime: getTime
	}

});