/**
 * Created by amixen on 23.12.2014.
 */
;
define(['../config', 'app/storage', 'knockout', 'knockout.Mapping'], function(config, storage, ko, koMapping){ //
	console.log('Tree loaded');

	function init(){
		console.log('Tree->init() start');

		var model;

		function TestViewModel() {
			var self = this;

			//console.log('storage.get(nodes): ', storage.get('nodes'));
			//var nodes = storage.get('nodes') ? storage.get('nodes') : config.defaults.nodes ;
			//console.log('nodes: ', nodes);
			//
			//self.nodes = ko.mapping.fromJSON(nodes);

			self.nodes = ko.observableArray();



			console.log('storage.get(nodes): ', storage.get('nodes'));
			var nodesT = storage.get('nodes') ? storage.get('nodes') : config.defaults.nodes ;
			console.log('nodesT: ', nodesT);

			//koMapping.fromJSON(nodesT, self.nodes, this);
			koMapping.fromJSON({nodes: nodesT}, this);
			console.log('self.nodes(): ', self.nodes());

			/*self.createNode = function(title, children){
				var selfSec = this;
				//console.log('title= ', title, ', children.length= ', children.length, ', children: ', children);
				selfSec.title = ko.observable(title);
				selfSec.children = ko.observableArray();
				if (children!==undefined && children.length) {
					//this.children = ko.observableArray(new self.createMenu(title, data));
					//foreach(children, function(item){
					//	this.children = new self.createNode(item.title, item.children);
					//});
					//for(){
					//
					//}
					children.forEach(function(item){
						console.log('item.title= ', item.title);
						//console.log('item.children.length= ', item.children.length, ', item.children: ', item.children);
						selfSec.children = new self.createNode(item.title, item.children);
					});
				}
			};*/

			//var userSettings = state.get("userSettings");
			//console.log('userSettings: ', userSettings);
			////state.set("userSettings", {name:'asd', email:'qwe@qwe.qwe', phone:123});
			////console.log('state.get("userSettings"): ', state.get("userSettings"));

			/*self.settingsEmail = ko.observable( (state.get("userSettings") != undefined) ? state.get("userSettings").email : null );
			self.settingsName = ko.observable( (state.get("userSettings") != undefined) ? state.get("userSettings").name : null );
			self.settingsPhone = ko.observable( (state.get("userSettings") != undefined) ? state.get("userSettings").phone : null );
			self.submitSettings = function(form){
				console.log('form= ', form);
				if ($(form).validate()) {
					//console.log('$(form).serialize(): ', $(form).serialize());
					console.log('$(form).serializeArray(): ', $(form).serializeArray());
					var formData = $(form).serializeArray();
					console.log('formData: ', formData);
					var formDataExt = {};
					//formDataExt = formData.map(function(item){
					//	//var newItem =
					//	//item['name'] =
					//	return {item['name']: item['value']};
					//});
					_.each($(form).serializeArray(), function(item){
						formDataExt[item.name] = item.value;
					});
					console.log('formDataExt: ', formDataExt);
					state.set('userSettings', formDataExt);
					console.log('state.get("userSettings"): ', state.get("userSettings"));
					//window.location.reload();
				}
			};*/

			/*$.getJSON('data/menu.json', function(data){
				console.log('data: ', data);
				self.menu(new self.createNode('root', data));
				console.log('self.menu(): ', self.menu());
			});*/




		}

		ko.applyBindings(model = new TestViewModel());

		//console.log('storage.get(nodes): ', storage.get('nodes'));
		//var nodes = storage.get('nodes') ? storage.get('nodes') : config.defaults.nodes ;
		//console.log('nodes: ', nodes);
		//
		//koMapping.fromJSON({nodes: nodes}, model);
		//console.log('model.nodes(): ', model.nodes());

	}

	return {
		init: init
	}
});