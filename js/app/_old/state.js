;
define(['../../lib/jquery', 'lib/jstorage'], function($, jstorage){

	console.log("Initialize State with ", $.jStorage, jstorage);

	return {
		  storage: $.jStorage

		, get: $.jStorage.get
		, set: function(key, value, options) { $.jStorage.set(key, value, options); return this; }
	}
});