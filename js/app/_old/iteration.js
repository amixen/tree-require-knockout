;
define(["../../lib/jquery", "underscore-loader", "knockout"],
	function ($, _, ko) {

		function init(options) {
			console.log("Init intro", options);

			var pageTitleInfo = 'Добро пожаловать к нам';

			/*var shortcuts=[
				{shortcut:'Alt+1', title:'Назначение'}
				,{shortcut:'Alt+2', title:'Дата'}
			];*/


			function TestViewModel() {
				var self = this;

				self.test = ko.observable();

				/*self.orderInfo = ko.observableArray();

				self.reasons = ko.observableArray([
					{id:1, name:'Добровольно за 24 часа до отправления'}
					,{id:2, name:'Добровольно за 16 часа до отправления'}
					,{id:3, name:'Добровольно за 10 часа до отправления'}
					,{id:4, name:'Добровольно за 8 часа до отправления'}
					,{id:5, name:'Добровольно за 6 часа до отправления'}
					,{id:6, name:'Добровольно за 4 часа до отправления'}
				]);
				self.reasonSelected = ko.observable(1);


				self.clickShowOrderInfo = function () {
					self.orderInfo.removeAll();
					console.log("self.orderNumber()= ", self.orderNumber());
					api.ask("findOrder", {}, {
						orderNumber: self.orderNumber()
					})
						.done(function(data){
							console.log("data: ", data);
							if (data.ticket==undefined) {
								data.ticket = [data];
							}
							var priceOrderAmount = 0;
							_.each(data.ticket, function(ticket){
								//console.log("data: ", data);
								self.orderInfo.push({
									systemNumber: ticket.system_number
									,ticketNumber: ticket.ticket_number
									,ticketStatus: ticket.ticket_status
									,ticketType: null
									,tripNumber: ticket.trip_number
									,startDate: ticket.start_date
									,numberSeat: ticket.number_seat
									,startCity: ticket.start_city
									,endCity: ticket.end_city
									,priceTotalAmount: ticket.total_amount + ' ' + ticket.sale_cur_code
								});
								priceOrderAmount += parseInt(ticket.total_amount);
							});
							console.log("priceOrderAmount= ", priceOrderAmount);
							self.priceOrderAmount(priceOrderAmount + ' ' + data.ticket[0].sale_cur_code);
						});
					//console.log("Cities request fired");
				};

				self.priceOrderAmount = ko.observable();

				self.clickOrderReturn = function () {
					var sendParams = {
						ticketCount: self.orderInfo().length
					};
					_.each(self.orderInfo(), function(item, index){
						sendParams['systemNumber'+index+''] = item.systemNumber;
						sendParams['returnReasonId'+index+''] = self.reasonSelected();
					});

					console.log('sendParams: ', sendParams);
					api.ask("returnPosition", {}, sendParams)
						.done(function(data){
							console.log("data: ", data);
							if (data.confirmation=="Y") {
								self.orderNumber(null);
								self.orderInfo.removeAll();
								self.priceOrderAmount(null);
								//self.orderCancelReason(null);
								alert("Возврат заказа прошел успешно");
							} else {
								alert("Возникли проблемы при возврате заказа.");
							}
						});
				};*/

			}


			if (!ko.components.isRegistered("intro")) {
				ko.components.register("intro", {
					viewModel: TestViewModel,
					template: { require: 'text!/layout/intro.html' }
				});
			}


			return {
				component: "intro"
				,pageTitleInfo: pageTitleInfo
				//,shortcuts: shortcuts
			};

			console.log("Intro module initialization done");
		}

		return {
			init: init
		};

	});
