;
define(['app/config', 'app/interface/list2', 'app/interface/list3'], function(config, list2, list3){

	function init(){
		//console.log('Interface->init() start');

		//list1.init();
		list2.init();
		list3.init();

		$(document).on('shown.bs.modal', config.modalId, function(){
			$(this).find('form input[name=text]').focus();
		});

		$(config.modalId)
			.on({
				dataSet: function(e, data){
					$(this).data(data);
					//console.log('dataSet end.');
				}
				,inputErasing: function(e){ //objList, keyAction
					$(this).data('objInput').focus().val(null);
					//console.log('inputErasing end.');
				}
				,dataLoadIntoInput: function(e){ //objList, keyAction
					$(this).trigger('inputErasing');
					$(this).data('objInput').val($(this).data('objListSpan').html());
					//console.log('dataLoadIntoInput end.');
				}
				,submit: function(e, tagId){
					e.preventDefault();
					var data = $(this).data();
					//console.log('data: ', data);
					if (data.keyAction=='edit'){
						data.objListSpan.html(data.objInput.val());
					} else if (['addInto','addNext'].indexOf(data.keyAction)!=-1){
						var li = data.objListSpan.closest('li');
						var tpl = '<li><span class="text">'+data.objInput.val()+'</span>'+
							'<div class="layerControls btn-group btn-group-xs">'+config.controls.map(function(item){return item}).join()+'</div>'+
						'</li>';
						if (data.keyAction=='addInto'){
							var obj = li.find('ul').eq(0);
							if (!obj.length) {
								obj = $('<ul></ul>');
								li.append(obj);
							}
							obj.append(tpl);
						} else {
							li.after(tpl);
						}
					}
					$(config.modalId).modal('hide');
					$(document).trigger('saveDom', data);
				}
			}, 'form');
	}

	return {
		  init: init
	};

});