/**
 * Created by amixen on 18.01.2015.
 */
;
define(['jquery', 'app/config', 'app/storage'], function($, config, storage){

	function tagEventHandlers(tagId, modalId, storageDataName){
		//console.log('tagId= ', tagId, ', modalId= ', modalId);
		$(tagId)
			.on({
				mouseover: function(e){
					e.stopPropagation();
					$(this).addClass('hover');
				}
				,mouseout: function(e){
					e.stopPropagation();
					$(this).removeClass('hover');
				}
			},'li')
			.on('click', '.layerControls button', function(e){
				var keyAction = $(this).attr('data-action');
				//console.log('Нажата клавиша: ', keyAction);
				var objList = $(this).closest('li');
				//console.log('objList: ', objList);
				var objListSpan = $(this).closest('li').find('span').eq(0);
				var objInput = $(modalId).find('form input[name=text]');

				if (['addInto','addNext','edit'].indexOf(keyAction)!==-1) {
					$(modalId).modal({keyboard: true, show: true});

					objInput.focus().val(null);

					$(modalId+' form')
						.trigger('dataSet', {
							storageDataName: storageDataName
							,tagId: tagId
							,keyAction: keyAction
							,objInput: objInput
							,objListSpan: objListSpan
						})
						.trigger('inputErasing');
					if (keyAction=='edit'){
						$(modalId+' form').trigger('dataLoadIntoInput');
					}
				} else if (keyAction=='remove') {
					if (window.confirm('Are you sure?')){
						objList.remove();
						$(document).trigger('saveDom', {storageDataName: storageDataName, tagId: tagId});
					}
				}
			});
	}

	function parseDomToJson(obj){
		if (obj!=undefined && obj.length){
			var elements = $(obj).find(">ul>li").toArray();
			//console.log('elements: ', elements);
			if (elements.length){
				return $.map(elements, function(item, index){
					//console.log('index= ', index, ', item: ', item);
					var array={};
					var span = $(item).find('.text');
					//console.log('span.eq(0)= ', span.eq(0), ', span.eq(0).html()= ', span.eq(0).html());
					array.text = $(item).find('.text').html();
					$(item).removeClass('hover');
					if ($(item).get(0).attributes.length){
						array['attributes'] = {};
						$.each($(item).get(0).attributes, function(index, item2){
							if (item2.nodeValue!='') {
								array.attributes[item2.nodeName] = item2.nodeValue;
							}
						});
					}
					if ($(item).find(">ul>li").length){
						array.children = parseDomToJson($(item));
					}
					return array;
				});
			}
		}
	}

	function parseJsonToDom(children){ // , obj
		var obj = null;
		if (children.length){
			obj = $('<ul></ul>');
			obj.append(children.map(function(item){
				var r = $('<li></li>')
						.attr((typeof item['attributes'] === "object") ? item['attributes'] : {} )
						.append('<span class="text">'+item.text+'</span>')
						.append('<div class="layerControls btn-group btn-group-xs">'+config.controls.map(function(item){return item}).join('')+'</div>')
					;
				if (item.children){
					r.append(parseJsonToDom(item.children));
				}
				return r;
			}));
		}
		return obj;
	}

	$(document).on('saveDom', function(e, data) {
		//console.log('data(saveDom): ', data);
		//var obj = $(data.tagId);
		var obj = $(data.tagId).clone();
		obj.find('.layerControls').remove();
		var dataJson = parseDomToJson(obj);
		//console.log(data.storageDataName, dataJson, ', будем сохранять..');
		storage.saveData(data.storageDataName, dataJson);
	});

	return {
		tagEventHandlers: tagEventHandlers
		,parseDomToJson: parseDomToJson
		,parseJsonToDom: parseJsonToDom
	}
});