/**
 * Created by amixen on 13.01.2015.
 */
;
define(['app/config'], function(config){
	//console.log('Storage loaded');

	function loadData(storageName){
		if (!storageName) storageName = config.defaults.storageDataName;
		var storage = window.localStorage;
		var data = storage.getItem(storageName) ? JSON.parse(storage.getItem(storageName)) : config.defaults.nodes ;
		//console.log('data: ', data);
		return data;
	}

	function saveData(storageName, data){
		if (!storageName) storageName = config.defaults.storageDataName;
		//console.log('Сохраняем в строке= ', JSON.stringify(data));
		window.localStorage.setItem(storageName, JSON.stringify(data));
	}

	function removeData(storageName, data){
		if (!storageName) storageName = config.defaults.storageDataName;
		window.localStorage.removeItem(storageName);
	}

	return {
		loadData: loadData
		,saveData: saveData
		,removeData: removeData
	}

});