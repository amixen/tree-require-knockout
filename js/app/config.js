;
//define(['app/state'], function(state){
define([], function(){
	//console.log('Config loaded');

	return {
		modalId: '#myModal'
		,controls: [
			'<button type="button" data-action="addNext" class="btn btn-default glyphicon glyphicon-plus-sign" title="Add next line"></button>'
			,'<button type="button" data-action="addInto" class="btn btn-default glyphicon glyphicon-plus" title="Add into"></button>'
			,'<button type="button" data-action="edit" class="btn btn-default glyphicon glyphicon-pencil" title="Edit"></button>'
			,'<button type="button" data-action="remove" class="btn btn-default glyphicon glyphicon-trash" title="Remove"></button>'
		]
		,defaults: {
			storageDataName: 'myData'
			,nodes: [
				{"text": "Menu 1"}
				,{"text": "Menu 2", "attributes": {"data-class":"child-2"}, "children": [
					{"text": "SubMenu 21"}
				]}
				,{"text": "Menu 3"}
			]
		}
	}
});