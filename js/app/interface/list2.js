/**
 * Created by amixen on 13.01.2015.
 */
;
define(['jquery', 'app/config', 'app/storage', 'app/api', 'bootstrap'], function($, config, storage, api){

	var timestamp = new Date();
	var tagId = '#list2';
	var timerId = '#list2-timer';

	function init(){
		//console.log('list2->init() start');
		//storage.removeData('dataJson');
		var dataFromStorage = storage.loadData('dataJson');
		var jqueryObject = api.parseJsonToDom(dataFromStorage);
		$(tagId).append(jqueryObject);

		api.tagEventHandlers(tagId, config.modalId, 'dataJson');

		$(timerId).html(Number(new Date() - timestamp)/1000 + ' ms');
	}



	return {
		init: init
	};

});