/**
 * Created by amixen on 13.01.2015.
 */
;
define(['jquery', 'app/config', 'app/storage', 'app/api', 'knockout', 'knockout.mapping'], function($, config, storage, api, ko, koMap){

	var timestamp = new Date();
	var tagId = '#list3';
	var timerId = '#list3-timer';

	function init(){
		//console.log('list3->init() start');

		api.tagEventHandlers(tagId, config.modalId, 'dataJsonKo');

		function TestViewModel() {
			var self = this;
			self.nodes = ko.observableArray(storage.loadData('dataJsonKo'));
			self.controls = ko.observable(config.controls.map(function(item){return item}).join());
			api.tagEventHandlers(tagId, config.modalId, 'dataJsonKo');
		}
		ko.applyBindings(new TestViewModel());

		$(timerId).html(Number(new Date() - timestamp)/1000 + ' ms');

	}

	return {
		init: init
	}
});