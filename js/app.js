/**
 * Created by amixen on 23.12.2014.
 */
;
//console.log('main loaded');
require.config({
	baseUrl: 'js'
	,paths: {
		jquery: 'lib/jquery'
		,bootstrap: 'lib/bootstrap'
		,"knockout"  : "lib/knockout"
		,"knockout.mapping"  : "lib/knockout.mapping"
	}
	,shim: {
		bootstrap: {
			deps: ['jquery']
		}
	}
});

define(['app/interface'], function(i){
	i.init();
});